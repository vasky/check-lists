import React, {useState} from 'react';
import './sass/app.sass';
import Sidebar from "./components/sidebar/sidebar";
import Input from "./components/ui/input/input";
import ModalCheckList from "./components/modalChecklist/modalCheckList";
import ContentHeader from "./components/contentCheckList/contentHeader/contentHeader";
import {useDispatch, useSelector} from "react-redux";
import {addElementCheckList, removeElementCheckList, toggleCheckStatusEl,
    rewriteCheckListName, rewriteCheckListDescription, rewriteElementTitle,
    makeNewCl, removeCl} from "./redux/todoSlice";

interface todoRedux {
    todo: Array<any>
}
function App() {
    const todo = useSelector(( state: todoRedux )=>state.todo)
    const dispatch = useDispatch()

    const [showModal, setShowModal] = useState(false)
    const [activeCL, setActiveCL] = useState(0)
    const [newElement, setNewElement] = useState('')

    const handlerChangeName = (name: string) => {
        dispatch(rewriteCheckListName({clId: activeCL, name}))
    }
    const handlerChangeDescription = (description: string) => {
        dispatch(rewriteCheckListDescription({clId: activeCL, description}))
    }

  return (
    <div className="App">
        <Sidebar
            setActiveCL={(index: number)=>setActiveCL(index)}
            checkLists={todo}
            onMakeNewCL={()=>setShowModal(true)} />
        <div className="content">
            {showModal && 
                <ModalCheckList 
                    makeNewCL={(name, description, icon, firstEl)=>dispatch(makeNewCl({name, description, icon, firstEl}))} 
                    onCloseClick={()=>setShowModal(false)} />}
            
            {/* Draw content if there is a checklist  */}
            {todo.length > 0 &&
            <>
                <ContentHeader
                    handlerChangeName={handlerChangeName}
                    handlerChangeDescription={handlerChangeDescription}
                    onClickRemoveCL={()=>dispatch(removeCl({clId: activeCL}))}
                    name={todo[activeCL].name}
                    description={todo[activeCL].description} />
                <div className="content__add-new-item">
                    <Input
                        onEnter={()=> {
                            // Add new todo item on press enter
                            dispatch(addElementCheckList({clId: activeCL, title: newElement}))
                        }}
                        onChange={(value: string) => setNewElement(value)}
                        greyBg={true} icon='plus' title='Добавьте новое задание' />
                </div>
                <div className="cl__element-list">
                    <ul className="cl__ul-list">
                        {todo[activeCL] && todo[activeCL].elements.map((el: {checked: boolean, title: string}, index: number) => {
                        const key = el.title + Math.random()

                        // return todo element item
                        return  (<li className="cl__element" key={key}>
                                    <input
                                        onClick={()=>dispatch(toggleCheckStatusEl({clId: activeCL, indexEl: index}))}
                                        type="checkbox" defaultChecked={el.checked} className="input__cl__element__checkbox" />
                                    <input
                                        onBlur={(e)=>{
                                            dispatch(rewriteElementTitle({clId: activeCL, indexEl: index, title: e.target.value}))
                                        }}
                                        defaultValue={el.title}
                                        placeholder="Пустое задание!"
                                        type="text"
                                        className='input__cl__element__main'/>
                                    <div
                                        onClick={()=>dispatch(removeElementCheckList({clId: activeCL, indexEl: index}))}
                                        className="cl__element__trash-icon">
                                        <img src="./imgs/icons/technical/trash_full.svg" alt=""/>
                                    </div>
                                </li> )
                        })}
                    </ul>
                </div>
            </>}
            {/* if there is no element, the block will be drawn with message */}
            {
                todo.length < 1  &&
               <h3>Создайте новый чек-лист!</h3>
            }
        </div>
    </div>
  );
}

export default App;

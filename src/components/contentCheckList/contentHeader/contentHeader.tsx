import React, {useEffect, useState} from 'react'

interface ContentHeaderProps{
    name: string,
    description: string,
    onClickRemoveCL: ()=>void,
    handlerChangeName: (name: string)=>void,
    handlerChangeDescription: (description: string)=>void
}
export default function ContentHeader(props: ContentHeaderProps) {
    useEffect(()=>{
        setInputName(props.name)
        setInputDescription(props.description)
    }, [props.name, props.description])

    const [inputName, setInputName] = useState(props.name)
    const [inputDescription, setInputDescription] = useState(props.name)

    function handlerChangeName(e: React.ChangeEvent<HTMLInputElement>) {
        const valueInput = e.target.value
        setInputName(valueInput)
        props.handlerChangeName(valueInput)
    }
    function handlerChangeDescription(e: React.ChangeEvent<HTMLInputElement>) {
        const valueInput = e.target.value
        setInputDescription(valueInput)
        props.handlerChangeDescription(valueInput)
    }
    return(
        <div className="content__header">

            <div className="cl-l1">
                <input
                    onChange={(e)=>handlerChangeName(e)}
                    type="text"
                    value={inputName}
                    placeholder='Введите название чек-листа'
                    className="input-cl__name"/>
                <div className="remove__check-list  with__notification-block">
                    <img 
                        onClick={props.onClickRemoveCL}
                        src="./imgs/icons/technical/trash_full.svg" alt=""/>
                    <div className="notification-block">
                        <p>Удалить чек-лист?</p>
                    </div>
                </div>
            </div>

            <div className="cl-l2">
                <div className="check-list__description">
                    <input
                        onChange={(e)=>handlerChangeDescription(e)}
                        type="text"
                        placeholder='Опишите ваш чек-лист'
                        value={inputDescription}
                        className="input-cl__description"/>
                </div>
            </div>
            
        </div>
    )
}
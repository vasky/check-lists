import React, {useState} from 'react'
import Input from "../ui/input/input";
import './modalCheckList.sass'
import Button from "../ui/button/button";
import Select from "../ui/select/select";
import classNames from "classnames";

interface ModalProps{
    onCloseClick: ()=>void,
    makeNewCL: (name: string, description: string, icon: string, firstEl: string)=>void
}

export default function ModalCheckList(props: ModalProps) {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [icon, setIcon] = useState<string>('')
    const [firstEl, setFirstEl] = useState<string>('')

    function handlerName(value: string): void{
        setName(value)
    }
    function handlerDescription(value: string): void{
        setDescription(value)
    }
    const selectIconsJsx = [
        <img src="./imgs/icons/another/school.svg" data-src-name='school' alt=""/>,
        <img src="./imgs/icons/another/calendar_today.svg" data-src-name='calendar_today' alt=""/>,
        <img src="./imgs/icons/another/error.svg" data-src-name='error' alt=""/>,
        <img src="./imgs/icons/another/smile.svg" data-src-name='smile' alt=""/>
    ]
    const selectIconsNames = [
        'school', 'calendar_today', 'error', 'smile'
    ]
    const clsButton = classNames({
        disabled: !description || !name || !icon || !firstEl 
    })
    function rebootModalForm() {
        setName('')
        setDescription('')
        setIcon('')
        setFirstEl('')
    }

    return(
        <div className="content__add-new__check-list">
            <div className="wrapper">
                <div className="modal__black-bg"></div>
                <div className="modal__body">

                    <div className="mb__header">
                        <h3>Создание чек-листа</h3>
                        {/* on click will be close a window */}
                        <div onClick={props.onCloseClick} className="close-modal__icon">
                            <img src="./imgs/icons/technical/close.svg" alt=""/>
                        </div>
                    </div>

                    <div className="mb__content">
                        <div className="new-check-list__properties">
                            <ul className="ncl__properties__ul">

                                <li className="ncl__properties__item">
                                    <Input onChange={(value)=>handlerName(value)} greyBg title='Название чек-листа' />
                                </li>
                                <li className="ncl__properties__item">
                                    <Input onChange={(value)=>handlerDescription(value)} greyBg title='Краткое описание' />
                                </li>
                                <li className="ncl__properties__item">
                                    <Select setIcon={(value: number)=>setIcon(selectIconsNames[value])} items={selectIconsJsx} />
                                </li>

                            </ul>
                        </div>

                        <div className="new-check-list__first-el">
                            <h3>Добавьте первое задание:</h3>
                            <Input onChange={(value)=>setFirstEl(value)} greyBg title='Что нужно сделать?' />
                        </div>
                        
                    </div>

                    <Button onClick={()=>{
                        props.makeNewCL(name, description, icon, firstEl)
                        props.onCloseClick()
                        rebootModalForm()
                        }} title='Вперед!' cls={clsButton} />
                </div>
            </div>
        </div>
    )
}
import React, {useState} from "react";
import './sidebar.sass'
import Input from "../ui/input/input";

interface sidebarProps {
    onMakeNewCL: ()=>void,
    checkLists?: Array<{
        name: string,
        description: string,
        elements: Array<{}>,
        icon: string
    }>,
    setActiveCL: (index: number)=>void
}

const Sidebar = (props: sidebarProps) => {
    
    const [inputSearch, setInputSearch] = useState('')

    return(
        <div className="sidebar">
            <div className="sidebar__header">
                <Input onChange={(value: string)=>setInputSearch(value)} icon='search' title='Введите название, описание' />
            </div>
            <div className="sidebar__body">
                <div className="sb__check-lists__catalog">
                    {props.checkLists &&
                    <ul className="check-lists__ul">
                        {props.checkLists.map((checkList, index) => {
                            
                            const groupElements = checkList.elements.reduce((prevValue: any, currentValue: any)=>{
                                prevValue += currentValue.title
                                return prevValue
                            }, '')
                            
                            if(!inputSearch || checkList.name.includes(inputSearch) || 
                            checkList.description.includes(inputSearch) || groupElements.includes(inputSearch))
                            return (
                                <li
                                    key={index + Math.random()}
                                    onClick={()=>props.setActiveCL(index)}
                                    className="check-list__item">
                                    <div className="item__wrapper">
                                        <div className="check-list__icon">
                                            <img src={`./imgs/icons/another/${checkList.icon}.svg`} alt=""/>
                                        </div>
                                        <div className="check-list__name">
                                            <h4>{checkList.name}</h4>
                                        </div>
                                        <div className="check-list__amount-tasks">{checkList.elements.length}</div>
                                    </div>
                                </li>
                            )
                            return ''
                        })
                        }
                    </ul>
                    }
                </div>
                <div className="sb__add-cl">
                    <div className="sb__add-cl__wrapper">
                        <div onClick={() => props.onMakeNewCL()} className="sb__add-cl__body">
                            <div className="icon" style={{'maskImage': './imgs/icons/technical/plus.svg'}} />
                            <div className="text-part"><h4>Добавить чек-лист</h4></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Sidebar
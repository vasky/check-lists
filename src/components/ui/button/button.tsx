import React from 'react'
import './button.sass'

interface ButtonProps{
    title: string,
    cls?: Object,
    onClick?: ()=>void
}
export default function Button(props: ButtonProps) {
    return (
        <div className={"button__wrapper " + props.cls}>
            <div 
                onClick={()=>{if(props.onClick)props.onClick()}}
                className="button__content">
                {props.title}
            </div>
        </div>
    )
}
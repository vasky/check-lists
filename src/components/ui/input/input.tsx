import React, { useState} from 'react'
import './input.sass'
import classNames from "classnames";

interface Props  {
    icon?: string,
    title: string,
    greyBg?: boolean,
    onChange?: (value: string)=>void,
    onEnter?: ()=>void
}

const Input = (props: Props) => {
    const cls = classNames({
        greyBg: props.greyBg
    })
    const handlerKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) =>{
        if(e.key === 'Enter'){
            if(props.onEnter && inputValue.length >= 5){
                props.onEnter()
                setInputValue('')
            }
        }
    }
    const [inputValue, setInputValue] = useState('')
    return(
        <div className={'input-wrapper ' + cls}>
            { props.icon &&
            <div className='icon'>
                <img alt={props.icon + '_icon'} src={`/imgs/icons/technical/${props.icon}.svg`}/>
            </div>}
            <input
                onKeyDown={handlerKeyDown}
                onChange={(e: React.ChangeEvent<HTMLInputElement>)=> {
                    if( props.onChange ){
                        props.onChange(e.target.value)
                    }
                    setInputValue(e.target.value)
                }}
                value={inputValue}
                type="text"
                placeholder={props.title}/>
        </div>
    )
}

export default  Input
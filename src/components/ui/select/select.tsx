import React, {useState} from 'react'
import './select.sass'
import classNames from "classnames";

interface SelectProps{
    items: Array<any>,
    setIcon?: (variable: number)=>void
}

export default function Select(props: SelectProps){
    const [open, setOpen] = useState(false)
    const [activeEl, setActiveEl] = useState<null | number >(null)
    
    const cls = classNames({ "select-opened": open })
    const activeElView = () => {
        if(activeEl !== null) {
            return props.items[activeEl]
        }else{
            return <p>Выберите иконку</p>
        }
    }

    // generate the select list
    return(
        <div className={'select-wrapper ' + cls}>
            <div onClick={()=> setOpen(!open)} className="select__header">
                <div className="select__value">
                    {activeElView()}
                </div>
                <div className="select__arrow-icon">
                    <img src="./imgs/icons/technical/arrow_forward_ios.svg" alt=""/>
                </div>
            </div>
            <div className="select__body">
                <div className="select__list">
                    <ul className="select__ul">
                        {
                            props.items && props.items.map((el, index) => {
                                return(
                                    <li
                                        key={index + Math.random()}
                                        onClick={()=> {
                                            setActiveEl(index)
                                            setOpen(false)
                                            if(props.setIcon)
                                            props.setIcon(index)
                                        }}
                                        className="select__item">
                                        {el}
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
}
import React from 'react';
import ReactDOM from 'react-dom';
import './sass/index.css';
import App from './App';
import './sass/fonts.sass'
import './sass/variables.sass'
import './sass/main.sass'
import {Provider} from 'react-redux'
import store from './redux/index'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);


import {combineReducers, configureStore} from '@reduxjs/toolkit'
import todoSlice from './todoSlice'

const rootReducer = combineReducers({
    todo: todoSlice
})

const store = configureStore({
    reducer: rootReducer
})

export default store
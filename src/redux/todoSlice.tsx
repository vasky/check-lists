import {createSlice} from "@reduxjs/toolkit";

let initialState = JSON.parse( localStorage.getItem("todo") || '[]')

if(initialState.length === 0){
    initialState = []
}

function resetLocalStorageState( state: Array<any> ) {
    localStorage.setItem('todo', JSON.stringify(state))
}

const todoSlice = createSlice({
    name: 'todo',
    initialState,
    reducers:{
        addElementCheckList(state, action){
            state[action.payload.clId].elements.push({
                checked: false,
                title: action.payload.title
            })
            resetLocalStorageState(state)
        },
        removeElementCheckList(state, action) {
            state[action.payload.clId].elements.splice(action.payload.indexEl, 1)
            resetLocalStorageState(state)
        },
        toggleCheckStatusEl(state, action) {
            state[action.payload.clId].elements[action.payload.indexEl].checked =
                !state[action.payload.clId].elements[action.payload.indexEl].checked
            resetLocalStorageState(state)
        },
        rewriteCheckListName(state, action) {
            state[action.payload.clId].name = action.payload.name
            resetLocalStorageState(state)
        },
        rewriteCheckListDescription(state, action) {
            state[action.payload.clId].description = action.payload.description
            resetLocalStorageState(state)
        },
        rewriteElementTitle(state, action) {
            state[action.payload.clId].elements[action.payload.indexEl].title = action.payload.title
            resetLocalStorageState(state)
        },
        makeNewCl(state, action) {
            state.push(
                {
                    name: action.payload.name,
                    description: action.payload.description,
                    icon: action.payload.icon,
                    elements: [
                        {
                            "checked": false,
                            "title": action.payload.firstEl
                        }
                    ]
                }
            )
            resetLocalStorageState(state)
        },
        removeCl(state, action) {
            state.splice(action.payload.clId, 1)
            resetLocalStorageState(state)
        }
    }
})

export default todoSlice.reducer
export const {addElementCheckList, removeElementCheckList, toggleCheckStatusEl,
    rewriteCheckListName, rewriteCheckListDescription, rewriteElementTitle,
    makeNewCl, removeCl} = todoSlice.actions